# vscode+opencv环境搭建(windows mingw )

#### 下载opencv源码

下载地址：[Releases - OpenCV](https://opencv.org/releases/)

我这里选择的是OpenCV – 4.5.5

![image-20230515204416077](./opencv.assets/image-20230515204416077.png)

解压源代码

![image-20230515204606134](./opencv.assets/image-20230515204606134.png)

在上一级创建build目录

![image-20230515204842613](./opencv.assets/image-20230515204842613.png)

#### 编译

打开cmake-gui.exe

![image-20230515204656788](./opencv.assets/image-20230515204656788.png)

填入源码路径和build路径

![image-20230515205042268](./opencv.assets/image-20230515205042268.png)

点击左下角Configure，选择MinGW Makefiles，点击Finish

![image-20230515205216503](./opencv.assets/image-20230515205216503.png)

#### 解决报错

等待结束之后可能是一片红色，多点几次，直到下方控制台的红色数量不再增加。(我到这里只剩下4处)

```
CMake Warning at cmake/OpenCVGenSetupVars.cmake:54 (message):
  CONFIGURATION IS NOT SUPPORTED: validate setupvars script in install
  directory
Call Stack (most recent call first):
  CMakeLists.txt:968 (include)
```

这个报错需要搜索框搜索 OPENCV_GENERATE_SETUPVARS 取消勾选

![image-20230515210448654](./opencv.assets/image-20230515210448654.png)



```
CMake Warning at cmake/OpenCVDownload.cmake:193 (message):
FFMPEG: Download failed: 7;"Couldn't connect to server"

For details please refer to the download log file:

D:/study/opencv_with_contrib_test_sln/CMakeDownloadLog.txt

Call Stack (most recent call first):
3rdparty/ffmpeg/ffmpeg.cmake:20 (ocv_download)
cmake/OpenCVFindLibsVideo.cmake:200 (download_win_ffmpeg)
CMakeLists.txt:636 (include)
```

剩下的三处报错都有`FFMPEG: Download failed: `，这是因为FFMPEG无法连接到下载服务器。

解决方法是通过外网下载好需要的文件，放到所需的目录。

---

打开`build/CMakeDownloadLog.txt`找到需要下载的文件链接和需要下载的目录

```
#do_copy "opencv_videoio_ffmpeg.dll" "eece4ec8304188117ffc7d5dfd0fc0ae" "https://raw.githubusercontent.com/opencv/opencv_3rdparty/4d348507d156ec797a88a887cfa7f9129a35afac/ffmpeg/opencv_videoio_ffmpeg.dll" "D:/GCC/opencv-4.5.5/build/3rdparty/ffmpeg"
#missing "D:/GCC/opencv-4.5.5/build/3rdparty/ffmpeg/opencv_videoio_ffmpeg.dll"
#check_md5 "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll"
#mismatch_md5 "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll" "d41d8cd98f00b204e9800998ecf8427e"
#delete "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll"
#cmake_download "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll" "https://raw.githubusercontent.com/opencv/opencv_3rdparty/4d348507d156ec797a88a887cfa7f9129a35afac/ffmpeg/opencv_videoio_ffmpeg.dll"
#try 1
# timeout on name lookup is not supported
# getaddrinfo(3) failed for raw.githubusercontent.com:443
# Could not resolve host: raw.githubusercontent.com
# Closing connection 0
# 
```

以第一段报错为例

最后一行显示了要下载的目录和下载地址

```
#cmake_download "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll" "https://raw.githubusercontent.com/opencv/opencv_3rdparty/4d348507d156ec797a88a887cfa7f9129a35afac/ffmpeg/opencv_videoio_ffmpeg.dll"
```

```
# 下载地址
https://raw.githubusercontent.com/opencv/opencv_3rdparty/4d348507d156ec797a88a887cfa7f9129a35afac/ffmpeg/opencv_videoio_ffmpeg.dll
# 下载到的目录
D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/
```

下载到本地后重命名文件

eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll

格式是 `文件MD5-文件名.dll` 

---

下载后的文件分别为

```
ffmpeg_version.cmake
opencv_videoio_ffmpeg.dll
opencv_videoio_ffmpeg_64.dll
```

如果也是4.5.5版本可以直接使用这个
[下载后的文件](https://gitee.com/yzhcat/opencv4.5.5_windows-mingw/tree/master/files/ffmpeg_files)


`ffmpeg_version.cmake`的链接打开后是一串文本![image-20230522194047254](./opencv.assets/image-20230522194047254.png)

不能复制保存，需要右键-->另存为-->TXT文件。然后删除后缀.txt。![image-20230522194215798](./opencv.assets/image-20230522194215798.png)

---

将文件复制到`.cache/ffmpeg/` 目录并按格式重命名

```
20deefbfe023c8b8d11a52e5a6527c6a-opencv_videoio_ffmpeg_64.dll
8862c87496e2e8c375965e1277dee1c7-ffmpeg_version.cmake
eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll
```

打开`.cache/ffmpeg/` 目录时，里面可能已经有文件了，但是大小是0KB。

文件的MD5可以直接使用0KB文件的文件名，也可以复制`#cmake_download "D:/GCC/opencv-4.5.5/opencv-4.5.5/.cache/ffmpeg/eece4ec8304188117ffc7d5dfd0fc0ae-opencv_videoio_ffmpeg.dll" `

里面的名称，或者找其他工具计算，顺便可以校验文件是否损坏。

![image-20230522195551733](./opencv.assets/image-20230522195551733.png)

#### 编译

修改完成后，回到CMake-gui，再次点击左下角Configure

![image-20230522201543286](./opencv.assets/image-20230522201543286.png)

没有报错，然后点击Configure右边的Generate。

完成后打开build目录，里面会有一个Makefile文件

![image-20230522201650967](./opencv.assets/image-20230522201650967.png)

在build目录打开cmd，执行`mingw32-make.exe -j 8`命令开始编译

![image-20230523123205459](./opencv.assets/image-20230523123205459.png)

编译完成后执行`mingw32-make.exe install`命令，生成库

然后到build找到install文件夹，复制出来。新建了一个文件夹，用来放install文件夹，其他文件可以删除了。

![新建了一个文件夹，放install文件夹](./opencv.assets/image-20230522201133228.png)

[编译后的文件](https://gitee.com/yzhcat/opencv4.5.5_windows-mingw/tree/master/files)

---

#### 配置环境变量

回到桌面，右键此电脑-->属性。找到高级系统设置，点击环境变量

![image-20230522202033686](./opencv.assets/image-20230522202033686.png)

在系统环境变量的Path里面添加install目录里面的bin文件夹路径

![image-20230522202126242](./opencv.assets/image-20230522202126242.png)

确认保存后重启电脑

#### 测试

打开cmd，输入`opencv_version`,显示opencv版本，说明环境配置成功。

![image-20230522202648930](./opencv.assets/image-20230522202648930.png)

准备一张图片，写个程序测试一下

![image-20230522203055648](./opencv.assets/image-20230522203055648.png)

```c++
#include "iostream"
#include "opencv2/opencv.hpp"

int main(int argc, char const *argv[])
{
	cv::Mat img = cv::imread("x1.png");
	if (img.empty())
	{
		std::cout << "image is empty or the path is invalid!" << std::endl;
		return 0;
	}
	cv::imshow("Origin", img);
	cv::waitKey(0);
	cv::destroyAllWindows();
	return 0;
}
```

编译命令

```bash
g++ ./hello_opencv.cpp -std=c++14 -I D:/GCC/opencv_4.5.5/install/include -L D:/GCC/opencv_4.5.5/install/x64/mingw/lib -lopencv_core455 -lopencv_imgcodecs455 -lopencv_imgproc455 -lopencv_calib3d455 -lopencv_dnn455 -lopencv_features2d455 -lopencv_flann455 -lopencv_gapi455 -lopencv_highgui455 -lopencv_ml455 -lopencv_objdetect455 -lopencv_photo455 -lopencv_stitching455 -lopencv_video455 -lopencv_videoio455 -o hello_opencv.exe
```

![image-20230522203244434](./opencv.assets/image-20230522203244434.png)

编译命令说明

`-I D:/GCC/opencv_4.5.5/install/include` 这里的路径是install目录下的头文件夹--include

`-L D:/GCC/opencv_4.5.5/install/x64/mingw/lib` 是install目录里面的库文件夹--bin

`-lopencv_core455` 这些参数后面的数字是opencv的版本，这里用的是4.5.5，

例如opencv4.5.2编译时，就要用` -lopencv_core452 -lopencv_imgcodecs452 ` 

#### 使用vscode编译

工作空间结构如下图

在`c_cpp_properties.json`里面添加头文件路径

![image-20230522203831060](./opencv.assets/image-20230522203831060.png)

```json
{
    "configurations": [
        {
            "name": "Win32",
            "includePath": [
                "${workspaceFolder}/**",
                "D:/GCC/opencv_4.5.5/install/include"
            ],
            "defines": [
                "_DEBUG",
                "UNICODE",
                "_UNICODE"
            ],
            "cStandard": "c17",
            "cppStandard": "c++17",
            "intelliSenseMode": "windows-gcc-x64",
            "compilerPath": "D:/GCC/MinGW64/bin/g++"
        }
    ],
    "version": 4
}
```

  配置tasks

  ![image-20230522204131583](./opencv.assets/image-20230522204131583.png)

  tasks和launch配置参考

```json
"launch": {
		"version": "0.2.0",
		"configurations": [
			{
				"name": "C/C++: cpp.exe 生成和调试活动文件OvO",
				"type": "cppdbg",
				"request": "launch",
				"program": "${workspaceFolder}/bin/a.exe",
				"args": [],
				"stopAtEntry": false,
				"cwd": "${workspaceFolder}/bin",
				"environment": [],
				"externalConsole": true,
				"MIMode": "gdb",
				"miDebuggerPath": "D:/GCC/MinGW64/bin/gdb.exe",
				"setupCommands": [
					{
						"description": "为 gdb 启用整齐打印",
						"text": "-enable-pretty-printing",
						"ignoreFailures": true
					},
					{
						"description": "将反汇编风格设置为 Intel",
						"text": "-gdb-set disassembly-flavor intel",
						"ignoreFailures": true
					}
				],
				"preLaunchTask": "C/C++: cpp.exe 生成活动文件"
			}
		]
	},
	"tasks": {
		"version": "2.0.0",
		"tasks": [
			{
				"type": "cppbuild",
				"label": "C/C++: cpp.exe 生成活动文件",
				"command": "g++",
				"args": [
					"-g",
					"${workspaceFolder}/src/${fileBasename}",
					"-o",
					"${workspaceFolder}/bin/a",
					"-mwindows",
					"-I",
					"${workspaceFolder}/include",
					"-I",
					"D:/GCC/opencv_4.5.5/install/include",
					"-L",
					"D:/GCC/opencv_4.5.5/install/x64/mingw/lib",
					"-lopencv_core455 ",
					"-lopencv_imgcodecs455 ",
					"-lopencv_imgproc455 ",
					"-lopencv_calib3d455 ",
					"-lopencv_dnn455 ",
					"-lopencv_features2d455 ",
					"-lopencv_flann455 ",
					"-lopencv_gapi455 ",
					"-lopencv_highgui455 ",
					"-lopencv_ml455 ",
					"-lopencv_objdetect455 ",
					"-lopencv_photo455 ",
					"-lopencv_stitching455 ",
					"-lopencv_video455 ",
					"-lopencv_videoio455",
					"-Wall"
				],
				"options": {
					"cwd": "D:/GCC/MinGW64/bin"
				},
				"problemMatcher": [
					"$gcc"
				],
				"group": {
					"kind": "build",
					"isDefault": true
				},
				"detail": "调试器生成的任务"
			}
		]
	}
```

也可以使用code-runner简单配置

![image-20230522204231378](./opencv.assets/image-20230522204231378.png)

```json
"settings": {
		"code-runner.executorMap": {
			"cpp": "cd $dir && g++ *.cpp -std=c++14 -mwindows -I D:/GCC/opencv_4.5.5/install/include -L D:/GCC/opencv_4.5.5/install/x64/mingw/lib -lopencv_core455 -lopencv_imgcodecs455 -lopencv_imgproc455 -lopencv_calib3d455 -lopencv_dnn455 -lopencv_features2d455 -lopencv_flann455 -lopencv_gapi455 -lopencv_highgui455 -lopencv_ml455 -lopencv_objdetect455 -lopencv_photo455 -lopencv_stitching455 -lopencv_video455 -lopencv_videoio455 -I$workspaceRoot/include -o $workspaceRoot/bin/a",
		},
}
```

使用`Ctrl+Shift+B`或code-runner编译，文件输出到bin目录。

将图片放到bin目录，双击a.exe运行。![image-20230522204832051](./opencv.assets/image-20230522204832051.png)
