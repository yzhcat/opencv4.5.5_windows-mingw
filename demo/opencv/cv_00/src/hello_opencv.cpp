#include "iostream"
#include "opencv2/opencv.hpp"

int main(int argc, char const *argv[])
{
	cv::Mat img = cv::imread("x1.png");
	if (img.empty())
	{
		std::cout << "image is empty or the path is invalid!" << std::endl;
		return 0;
	}
	cv::imshow("Origin", img);
	cv::waitKey(0);
	cv::destroyAllWindows();
	return 0;
}