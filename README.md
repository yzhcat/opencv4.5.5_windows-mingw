# opencv4.5.5_windows mingw

#### 介绍

vscode+opencv环境搭建(windows mingw )

- 环境搭建方法
- 编译所需文件
- 编译后的文件
- 测试文件

#### 文件说明

- demo 测试文件
- doc 环境搭建说明 [vscode+opencv环境搭建](https://gitee.com/yzhcat/opencv4.5.5_windows-mingw/blob/master/doc/opencv.md)(windows mingw )
- files ffmpeg需要的文件，编译后的文件